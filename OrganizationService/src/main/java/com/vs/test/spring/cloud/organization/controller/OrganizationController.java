package com.vs.test.spring.cloud.organization.controller;

import com.vs.test.spring.cloud.organization.client.EmployeeClient;
import com.vs.test.spring.cloud.organization.pojo.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class OrganizationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrganizationController.class);

	@Autowired
	private EmployeeClient employeeClient;

	@GetMapping("/organization/{organizationId}")
	public Organization findByOrganization(@PathVariable("organizationId") Long organizationId) {
		LOGGER.info("Organization find: organizationId={}", organizationId);
		List<Long> employeesIds = Arrays.asList(1L, 2L, 3L);
		Organization organization = new Organization();
		organization.setId(organizationId);
		for (Long employeeId : employeesIds) {
			organization.getEmployees().add(employeeClient.findById(employeeId));
		}
		return organization;
	}
}
