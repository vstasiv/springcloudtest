package com.vs.test.spring.cloud.organization.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "employee-service")
public interface EmployeeClient {
	@GetMapping("/employee/{id}")
	String findById(@PathVariable("id") Long id);
}
