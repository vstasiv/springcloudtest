package com.vs.test.spring.cloud.organization.pojo;

import java.util.ArrayList;
import java.util.List;

public class Organization {
	private Long id;
	private final List<String> employees = new ArrayList<>();

	public List<String> getEmployees() {
		return employees;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
