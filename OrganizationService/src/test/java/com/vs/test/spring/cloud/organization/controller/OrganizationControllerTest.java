package com.vs.test.spring.cloud.organization.controller;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import com.vs.test.spring.cloud.organization.OrganizationApplication;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
//import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {OrganizationApplication.class, OrganizationControllerTest.LocalRibbonClientConfiguration.class})
@AutoConfigureMockMvc
public class OrganizationControllerTest {
	
	@SuppressWarnings("SpringJavaAutowiringInspection")
	@Autowired
	private MockMvc mockMvc;

	@ClassRule
	public static WireMockClassRule wiremock = new WireMockClassRule(wireMockConfig().dynamicPort());

	@Test
	public void shouldReturnOrganisationWithEmployees() throws Exception {
		int organisationId = 1111;
		int employeeId1 = 1;
		int employeeId2 = 2;
		int employeeId3 = 3;
		String expectedOrganisationJSon = "{" +
				"\"id\": " + organisationId + "," +
				"\"employees\": [" +
				"    \"Employee #" + employeeId1 + "\"," +
				"    \"Employee #" + employeeId2 + "\"," +
				"    \"Employee #" + employeeId3 + "\"" +
				"  ]" +
				"}";

		stubForEmployeeService(employeeId1);
		stubForEmployeeService(employeeId2);
		stubForEmployeeService(employeeId3);

		mockMvc.perform(MockMvcRequestBuilders.get("/organization/" + organisationId))
				.andExpect(status().isOk())
				.andExpect(content().json(expectedOrganisationJSon));
	}

	private void stubForEmployeeService(int employeeId) {
		stubFor(get(urlPathEqualTo("/employee/" + employeeId))
				.willReturn(aResponse().withHeader("Content-Type", "text/plain").withBody("Employee #" + employeeId)));
	}

	@TestConfiguration
	public static class LocalRibbonClientConfiguration {
		@Bean
		public ServerList<Server> ribbonServerList() {
			return new StaticServerList<>(new Server("localhost", wiremock.port()));
		}
	}
}