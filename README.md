It is a simple prototype application based on spring cloud.

Prototype consists of 5 modules:
1. ConfigService - this service is used by other services to fetch their configs
2. ServiceDiscovery - this service is used to register all managed services to communicate each other through it
3. EmployeeService - managed micro-service to get info about employee
4. OrganisationService - managed micro-service to get info about organisation and employees there
5. GatewayService - it aims to provide a simple, yet effective way to route to APIs and provide cross cutting concerns to them such as: security, monitoring/metrics, and resilience.

**Prerequirements**

* Java 8 or later
* Maven 3.2+

**How to run it in Windows:**

1. git clone https://vstasiv@bitbucket.org/vstasiv/springcloudtest.git
2. cd SpringCloudTest
3. cd ConfigService && mvn spring-boot:run
4. cd ..\ServiceDiscavery && mvn spring-boot:run
5. cd ..\EmployeeService && mvn spring-boot:run
6. cd ..\OrganisationService && mvn spring-boot:run
7. cd ..\GatewayService && mvn spring-boot:run

**How to check it:**

* Just open browser and go to :
      1. http://localhost:8081 - ServiceDiscovery application.
          * It is an spring eureka admin console where you will see information about all services that have been registered and server info: memory, env, cpu and etc.
      2. http://localhost:8090/employee/<EMPLOYEE_ID> - EmployeeService application.
          * Replace <EMPLOYEE_ID> with any integer. Will return simple information about employee
          * http://localhost:8090/v2/api-docs - API documentation
          * http://localhost:8090/swagger-ui.html - Human readable API documentation
      3. http://localhost:8091/organization/<ORGANISATION_ID> - OrganisationService application.
          * Replace <ORGANISATION_ID> with any integer. Will return simple information about organisation and employees there
          * OrganisationService fetches information about EmployeeService such as: url and capabilities, and after that goes to EmployeeService to retrieve information about employee
          * http://localhost:8091/v2/api-docs - API documentation
          * http://localhost:8091/swagger-ui.html - Human readable API documentation
      4. http://localhost:8060/<URI> - GatewayService application.
          * It is just a router to *EmployeeService* and *OrganisationService*. You should use the same URLs for these services that have been described above and just replace port number to *8060*
      