package com.vs.test.spring.cloud.employee.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

	@GetMapping("/employee/{id}")
	public String findById(@PathVariable("id") Long id) {
		LOGGER.info("Employee find: id={}", id);
		return "Employee #" + id;
	}
}